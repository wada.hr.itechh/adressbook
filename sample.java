//　入力された住所録をテキストファイルに保存
//testesttest
//*********************************//
//　enshu1.javaと同じ入力方法でお願いします。//
//*********************************//
package enshu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;

/**
 * 【住所録を新規登録or検索するシステム】
 * @author WadaHiroki
 */
public class enshu2
{
	/**
	 * 【メインメニューでの選択】
	 * @param args
	 * @throws IOException
	 * @return 【無し】
	 */
	public static void main(String[] args) throws IOException
	{		
		String str_filename = "enshu2.txt";
		File newfile = new File(str_filename);
		if(newfile.exists()) {
		}
		else {
			newfile.createNewFile();
		}

		System.out.println("------------------");
		System.out.println("1	:住所録登録");
		System.out.println("2	:住所録検索");
		System.out.println("それ以外	:閉じる");
		System.out.println("------------------");

		BufferedReader br_sentaku = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("番号を選択してください。");
		String str = br_sentaku.readLine();
		
		//　空白の場合は再入力
		int BangoHanteiFlag = 0;
		while(BangoHanteiFlag != 1) {
			if(str.isEmpty()) {
				System.out.println("空白無効");
				System.out.println("番号を選択してください。");
				str = br_sentaku.readLine();
			}
			else {
				BangoHanteiFlag = 1;
			}
		}
		
		//　入力文字の判定
		if((str.equals("1")) || (str.equals("１"))){
			input(str_filename);
			main(args);
		}
		
		else if((str.equals("2")) || (str.equals("２"))){
			search(str_filename);
			main(args);
		}
		
		else {
			System.out.println("終了します。");
			System.exit(0);
		}
		
		return;
	}

	/**
	 * 【住所録の内容を一つずつ登録】
	 * @param str_filename 【登録するファイル名】
	 */
	public static void input(String str_filename) throws IOException {
		//　登録情報
		System.out.println("\n住所録の登録をします。");
		
		ArrayList<String> yoso = new ArrayList<>(6);
		String id ;
		String name;
		String kana;
		String postcode;
		String adr;
		String tel;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("\n住所録番号：自動入力されます");
		//　ファイルを読み込み住所録末端番号+1する
		int jusho_cnt = 0;
		try(FileReader fr_id = new FileReader(str_filename);
				BufferedReader br_id = new BufferedReader(fr_id)){

			String jushoroku_hairetu = null;
			jushoroku_hairetu = br_id.readLine();		
			while(jushoroku_hairetu != null) {
				jusho_cnt++;
				jushoroku_hairetu = br_id.readLine();
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}

		if(jusho_cnt == 0) {
			jusho_cnt = 1;
			id = Integer.toString(jusho_cnt);
			yoso.add(id);
		}
		else {
			jusho_cnt++;
			id = Integer.toString(jusho_cnt);
			yoso.add(id);
		}
		
		System.out.println("\n名前を入力してください。[数字は無効]");
		name = br.readLine();
		name = name_check(name);
		yoso.add(name);	
		
		System.out.println("ふりがなを入力してください。[平仮名のみ有効]");
		kana = br.readLine();
		kana = kana_check(kana);		
		yoso.add(kana);
		
		System.out.println("郵便番号を入力してください。[数字7桁]");
		postcode = br.readLine();
		postcode = postcode_check(postcode);
		yoso.add(postcode);
		
		System.out.println("住所を入力してください。");
		adr = br.readLine();
	    adr = adr_check(adr);
		yoso.add(adr);
		
		System.out.println("電話番号を入力してください。[数字 10 or 11 桁]");
		tel = br.readLine();
		tel = tel_check(tel);
		yoso.add(tel);
		
		touroku_kakunin(id, name, kana, postcode, adr, tel, str_filename);
		return;	
	}
	
	/**
	 * 【登録内容の確認・修正を行う】
	 * @param id　【住所録番号】
	 * @param name 【名前】
	 * @param kana 【ふりがな】
	 * @param postcode 【郵便番号】
	 * @param adr 【住所】
	 * @param tel 【電話番号】
	 * @param str_filename 【登録しているファイル名】
	 * @throws IOException
	 */
	static void touroku_kakunin(String id, String name, String kana, String postcode, String adr, String tel, String str_filename) throws IOException {
		ArrayList<String> yoso = new ArrayList<>(6);
		int TourokuKakuninFlag = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while(TourokuKakuninFlag == 0) {
			System.out.println(String.format("\n住所録番号	:%s", id));
			System.out.println(String.format("名前		:%s", name));
			System.out.println(String.format("ふりがな		:%s", kana));
			System.out.println(String.format("郵便番号		:%s", postcode));
			System.out.println(String.format("住所		:%s", adr));    					
			System.out.println(String.format("電話番号		:%s\n", tel));
			
			//　登録情報が正しいかどうかの確認
			String kakunin = "y";
			System.out.println("\n登録情報は正しいですか?");
			System.out.println("正しい⇒yと入力してください");
			System.out.println("間違い⇒y以外で入力してください");
			BufferedReader br_kakunin = new BufferedReader(new InputStreamReader(System.in));
			String str_kakunin = br_kakunin.readLine();
			if(str_kakunin.equals(kakunin)) {
				//　登録情報が正しいので住所録番号をファイルに追記
				FileWriter file = new FileWriter(str_filename, true);
				PrintWriter pw = new PrintWriter(new BufferedWriter(file));
				pw.printf(id);
				pw.printf(" " + name);
				pw.printf(" " + kana);
				pw.printf(" " + postcode);
				pw.printf(" " + adr);
				pw.println(" " + tel);
				pw.close();
				
				TourokuKakuninFlag = 1;
				System.out.println("登録しました。");
				System.out.println("現在の登録総件数 : " + id);
				
				return;
			}
			else {
				//　登録せず、再入力を行う
				System.out.println("どの入力項目を修正しますか？");
				System.out.println("名前		　⇒　1を入力してください。");
				System.out.println("ふりがな		　⇒　2を入力してください。");
				System.out.println("郵便番号		　⇒　3を入力してください。");
				System.out.println("住所		　⇒　4を入力してください。");
				System.out.println("電話番号		　⇒　5を入力してください。");
				
				BufferedReader br_syuseibango = new BufferedReader(new InputStreamReader(System.in));

				System.out.println("番号を選択してください。[1〜5の数字のみ有効]");
				String str_syuseibango = br_syuseibango.readLine();
				//　空白の場合は再入力
				int SainyuryokuHanteiFlag = 0;
				while(SainyuryokuHanteiFlag != 1) {
					if(str_syuseibango.isEmpty()) {
						System.out.println("空白無効");
						System.out.println("再入力してください。[1〜5の数字のみ有効]");
						str_syuseibango = br_syuseibango.readLine();
					}
					else {
						//　1~5以外の場合も再入力
						if((!str_syuseibango.equals("1")) && (!str_syuseibango.equals("１")) && (!str_syuseibango.equals("2")) && (!str_syuseibango.equals("２")) && (!str_syuseibango.equals("3")) && (!str_syuseibango.equals("３")) && (!str_syuseibango.equals("4")) && (!str_syuseibango.equals("４")) && (!str_syuseibango.equals("5")) && (!str_syuseibango.equals("５"))) {
							System.out.println("無効入力");
							System.out.println("再入力してください。[1〜5の数字のみ有効]");
							str_syuseibango = br_syuseibango.readLine();
						}
						else {
							SainyuryokuHanteiFlag = 1;
						}
					}
				}
				
				//　入力文字の判定
				if((str_syuseibango.equals("1")) || (str_syuseibango.equals("１"))){
					System.out.println("\n名前を入力してください。[数字は無効]");
					name = br.readLine();
					name = name_check(name);
					yoso.add(name);
				}
				
				else if((str_syuseibango.equals("2")) || (str_syuseibango.equals("２"))){
					System.out.println("ふりがなを入力してください。[平仮名のみ有効]");
					kana = br.readLine();
					kana = kana_check(kana);		
					yoso.add(kana);
				}
				
				else if((str_syuseibango.equals("3")) || (str_syuseibango.equals("３"))){
					System.out.println("郵便番号を入力してください。[数字7桁]");
					postcode = br.readLine();
					postcode = postcode_check(postcode);
					yoso.add(postcode);
				}
				
				else if((str_syuseibango.equals("4")) || (str_syuseibango.equals("４"))) {
					System.out.println("住所を入力してください。");
					adr = br.readLine();
				    adr = adr_check(adr);
					yoso.add(adr);
				}
				
				else if((str_syuseibango.equals("5")) || (str_syuseibango.equals("５"))) {
					System.out.println("電話番号を入力してください。[数字 10 or 11 桁]");
					tel = br.readLine();
					tel = tel_check(tel);
					yoso.add(tel);
				}
			}
		}
		return;
	}
	
	/**
	 * 【名前の正当性を確認する】
	 * @param name 【名前】
	 * @return 【登録する名前】
	 * @throws IOException
	 */
	static String name_check(String name) throws IOException {
		int NameFlag = 0;
		int name_i, NameHanteiFlag;
		while(NameFlag == 0) {
			NameHanteiFlag = 0;
			
			//　空欄かどうかのチェック
			if(name.length() == 0) {
				System.out.println("error:空白です。");
				System.out.println("名前を入力してください。[数字は無効]");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				name = br.readLine();
				NameHanteiFlag = 1;
			}
			
			for(name_i = 0; name_i < name.length(); name_i++) {
				//　文字false 数字true
				if(Character.isDigit(name.charAt(name_i))) {
					System.out.println("error:数字が入力されています。");
					System.out.println("名前を入力してください。[数字は無効]");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					name = br.readLine();
					NameHanteiFlag = 1;
					break;
				}
			}
			if(NameHanteiFlag == 0) {
				NameFlag = 1;
				continue;
			}
		}
		return name;
	}
	
	/**
	 * 【ふりがなの正当性を確認する】
	 * @param kana 【ふりがな】
	 * @return 【登録するふりがな】
	 * @throws IOException
	 */
	static String kana_check(String kana) throws IOException {
		int KanaFlag = 0;
		int kana_i, KanaHanteiFlag;
		while(KanaFlag == 0) {
			KanaHanteiFlag = 0;

			//　空欄かどうかのチェック
			if(kana.length() == 0) {
				System.out.println("error:空白です。");
				System.out.println("ふりがなを入力してください。[平仮名のみ有効]");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				kana = br.readLine();
				KanaHanteiFlag = 1;
			}

			for(kana_i = 0; kana_i < kana.length(); kana_i++) {
				//　文字false 数字true
				if(Character.isDigit(kana.charAt(kana_i))) {
					System.out.println("error:数字が入力されています。");
					System.out.println("ふりがなを入力してください。[平仮名のみ有効]");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					kana = br.readLine();
					KanaHanteiFlag = 1;
					break;
				}
			}

			//　平仮名false
			if (!kana.matches("^[\\u3040-\\u309F]+$")) {
				System.out.println("error:平仮名以外が入力されています。");
				System.out.println("ふりがなを入力してください。[平仮名のみ有効]");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				kana = br.readLine();
				KanaHanteiFlag = 1;
			}

			if(KanaHanteiFlag == 0) {
				KanaFlag = 1;
				continue;
			}
		}
		return kana;
	}
	
	/**
	 * 【郵便番号の正当性を確認する】
	 * @param postcode 【郵便番号】
	 * @return 【登録する郵便番号】
	 * @throws IOException
	 */
	static String postcode_check(String postcode) throws IOException {
		int PostcodeFlag = 0;
		int postcode_i, PostcodeHanteiFlag;
		while(PostcodeFlag == 0) {
			PostcodeHanteiFlag = 0;

			//　空欄かどうかのチェック
			if(postcode.length() == 0) {
				System.out.println("error:空白です。");
				System.out.println("郵便番号を入力してください。[数字7桁]");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				postcode = br.readLine();
				PostcodeHanteiFlag = 1;
			}

			for(postcode_i = 0; postcode_i < postcode.length(); postcode_i++) {
				//　文字true 数字false
				if(!Character.isDigit(postcode.charAt(postcode_i))) {
					System.out.println("error:文字が入力されています。");
					System.out.println("郵便番号を入力してください。[数字7桁]");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					postcode = br.readLine();
					PostcodeHanteiFlag = 1;
					break;
				}

				//　7文字出ない場合再入力
				if(postcode.length() != 7) {
					System.out.println("error:" + postcode.length() + "桁になっています。7桁で入力してください。");
					System.out.println("郵便番号を入力してください。[数字7桁]");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					postcode = br.readLine();
					PostcodeHanteiFlag = 1;
					break;
				}
			}

			if(PostcodeHanteiFlag == 0) {
				PostcodeFlag = 1;
				continue;
			}
		}
		return postcode;
	}
	
	/**
	 * 【住所の正当性を確認する】
	 * @param adr 【住所】
	 * @return 【登録する住所】
	 * @throws IOException
	 */
	static String adr_check(String adr) throws IOException {
		int AdrFlag = 0;
		int AdrHanteiFlag;
		while(AdrFlag == 0) {
			AdrHanteiFlag = 0;

			//　空欄かどうかのチェック
			if(adr.length() == 0) {
				System.out.println("error:空白です。");
				System.out.println("住所を入力してください。");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				adr = br.readLine();
				AdrHanteiFlag = 1;
			}

			if(AdrHanteiFlag == 0) {
				AdrFlag = 1;
				continue;
			}
		}
		return adr;
	}
	
	/**
	 * 【電話番号の正当性を確認する】
	 * @param tel 【電話番号】
	 * @return 【登録する電話番号】
	 * @throws IOException
	 */
	static String tel_check(String tel) throws IOException {
		int TelFlag = 0;
		int tel_i, TelHanteiFlag;
		while(TelFlag == 0) {
			TelHanteiFlag = 0;

			//　空欄かどうかのチェック
			if(tel.length() == 0) {
				System.out.println("error:空白です。");
				System.out.println("電話番号を入力してください。[数字 10 or 11 桁]");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				tel = br.readLine();
				TelHanteiFlag = 1;
			}

			for(tel_i = 0; tel_i < tel.length(); tel_i++) {
				//　文字true 数字false
				if(!Character.isDigit(tel.charAt(tel_i))) {
					System.out.println("error:文字が入力されています。");
					System.out.println("電話番号を入力してください。[数字 10 or 11 桁]");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					tel = br.readLine();
					TelHanteiFlag = 1;
					break;
				}

				//　10桁もしくは11桁でない場合再入力
				if(tel.length() != 10) {
					if(tel.length() != 11) {
						System.out.println("error:" + tel.length() + "桁になっています。10桁または11桁で入力してください。");
						System.out.println("電話番号を入力してください。[数字 10 or 11 桁]");
						BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
						tel = br.readLine();
						TelHanteiFlag = 1;
						break;
					}
				}
			}

			if(TelHanteiFlag == 0) {
				TelFlag = 1;
				continue;
			}
		}
		return tel;
	}

	/**
	 * 【ファイルから住所録を検索する】
	 * @param str_filename　【住所録を保存しているファイル名】
	 * @throws IOException
	 */
	static void search(String str_filename) throws IOException {		
		//　ファイルから呼出を行う。
		System.out.println("検索する住所録番号を入力してください。 ");
		BufferedReader num = new BufferedReader(new InputStreamReader(System.in));
		String kensaku = num.readLine();

		int KensakuHanteiFlag = 0;
		try(FileReader fr_kensaku = new FileReader(str_filename);
				BufferedReader br_kensaku = new BufferedReader(fr_kensaku)){

			//　配列の初期化
			String jushoroku_hairetu = null;
			//　1行ずつ住所録情報を取り出す
			jushoroku_hairetu = br_kensaku.readLine();
			while(jushoroku_hairetu != null) {
				//　配列の1文字目を抽出する⇒変数kirinukiに格納
				int index1 = jushoroku_hairetu.indexOf(" ");
				String kirinuki = jushoroku_hairetu.substring(0,index1);
				
				//　配列要素1つ目と検索する住所録番号の比較
				if(kensaku.equals(kirinuki)) {
					//　配列を特定文字で切り分ける
					//　今回は空白で配列を切り分ける
					String[] new_hairetu = jushoroku_hairetu.split(" ");

					for ( int j = 0; j < 6; j++ ){
						switch(j) {
						case 0:
							System.out.println(String.format("\n住所録番号	:%s", new_hairetu[0]));
							break;
						case 1:
							System.out.println(String.format("名前		:%s", new_hairetu[1]));
							break;
						case 2:
							System.out.println(String.format("ふりがな		:%s", new_hairetu[2]));
							break;
						case 3:
							System.out.println(String.format("郵便番号		:%s", new_hairetu[3]));
							break;
						case 4:
							System.out.println(String.format("住所		:%s", new_hairetu[4]));
							break;
						case 5:   					
							System.out.println(String.format("電話番号		:%s\n", new_hairetu[5]));
							break;
						}
					}
					KensakuHanteiFlag = 1;
				}
				
				jushoroku_hairetu = br_kensaku.readLine();
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}
		
		if(KensakuHanteiFlag == 0) {
			System.out.println("\n住所録番号「 " + kensaku + " 」は存在しません。\n");
		}
		
		return;
	}
}